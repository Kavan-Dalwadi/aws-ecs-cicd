# Prerequisites
# Must have aws cli configure and docker daemon running in cicd tool's user e.g. jenkins or gitlab-runner
# Must have **jq** installed on runner
# Login into AWS ECR using docker before running this script


#/bin/bash/

export ver=$(cat ./version)
docker build -t 808755661750.dkr.ecr.ap-south-1.amazonaws.com/nodejs:latest .
docker push 808755661750.dkr.ecr.ap-south-1.amazonaws.com/nodejs:latest

docker tag 808755661750.dkr.ecr.ap-south-1.amazonaws.com/nodejs:latest 808755661750.dkr.ecr.ap-south-1.amazonaws.com/nodejs:$ver
docker push 808755661750.dkr.ecr.ap-south-1.amazonaws.com/nodejs:$ver

docker rmi 808755661750.dkr.ecr.ap-south-1.amazonaws.com/nodejs:latest
docker rmi 808755661750.dkr.ecr.ap-south-1.amazonaws.com/nodejs:$ver
