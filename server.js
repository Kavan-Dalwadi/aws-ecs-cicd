'use strict';

const express = require('express');

// Constants
const PORT = 3000;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
  res.send('Hii welcome to SA--devops and its VER-0.1.3');
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
