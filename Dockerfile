FROM node:14-alpine as stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .


FROM node:alpine as main
COPY --from=stage /app ./
EXPOSE 3000
CMD [ "node", "server.js" ]
